"""Volume I Lab 18: Profiling and Optimizing Python Code
Lehner White
Math 347
Feb 23 2016
"""
from time import time
import numpy as np
from scipy import linalg
from numba import jit
from copy import copy

# Problem 1
def compare_timings(f, g, *args):
    """Compares the timings of 'f' and 'g' with arguments '*args'.

    Inputs:
        f (callable): first function to compare.
        g (callable): second function to compare.
        *args (any type): arguments to use when callings functions
            'f' and 'g'
    Returns:
        comparison (string): The comparison of the runtimes of functions
            'f' and 'g' in the following format :
                Timing for <f>: <time>
                Timing for <g>: <time>
            where the values inside <> vary depending on the inputs)
    """ 
    f_start = time()
    f(*args)
    f_stop = time()
    f_lapsed = f_stop - f_start
    
    g_start = time()
    g(*args)
    g_stop = time()
    g_lapsed = g_stop - g_start

    comparison = 'Timing for {}: {}\nTiming for {}: {}'.format(str(f), str(f_lapsed),str(g), str(g_lapsed))
    return comparison

def test1():
    def LU(A):
        """Returns the LU decomposition of a square matrix."""
        n = A.shape[0]
        U = np.array(np.copy(A), dtype=float)
        L = np.eye(n)
        for i in range(1,n):
            for j in range(i):
                L[i,j] = U[i,j]/U[j,j]
                for k in range(j,n):
                    U[i,k] -= L[i,j] * U[j,k]
        return L, U
    A = np.random.randint(10,size=(5,5))
    
    print compare_timings(linalg.lu, LU, A)
#test1() 
    
# Problem 2
def LU(A):
    """Returns the LU decomposition of a square matrix."""
    n = A.shape[0]
    U = np.array(np.copy(A), dtype=float)
    L = np.eye(n)
    for i in range(1,n):
        for j in range(i):
            L[i,j] = U[i,j]/U[j,j]
            for k in range(j,n):
                U[i,k] -= L[i,j] * U[j,k]
    return L,U

def LU_opt(A):
    """Returns the LU decomposition of a square matrix."""
    n = A.shape[0]
    U = np.array(np.copy(A), dtype=float)
    L = np.eye(n)
    for i in range(1,n):
        for j in range(i):
            L[i,j] = U[i,j]/U[j,j]
            U[i,j:n] -= L[i,j] * U[j,j:n]
    return L,U

def compare_LU(A):
    """Prints a comparison of LU and LU_opt with input of the matrix A."""
    print compare_timings(LU, LU_opt, A)
#compare_LU(np.random.randint(10,size=(300,300)))

# Problem 3
def mysum(X):
    """Return the sum of the elements of X.
    Inputs:
        X (array) - a 1-D array
    """
    tot = 0
    for x in X:
        tot += x
    return tot

def compare_sum(X):
    """Prints a comparison of mysum and sum and prints a comparison
    of mysum and np.sum."""
    print compare_timings(mysum, sum, X)
    print compare_timings(mysum, np.sum, X)
#compare_sum([x for x in xrange(100000)])
#compare_sum(np.arange(100000))

# Problem 4
def fib(n):
    """A generator that yields the first n Fibonacci numbers."""
    xk = 0
    xk1 = 1
    if n >= 0:  
        yield xk
    if n >= 1:
        yield xk1
    if n > 1:
        for i in xrange(n):
            yield (xk + xk1)
            xk, xk1 = xk1, (xk + xk1)
def test4():
    for x in fib(10):
        print x

# Problem 5
def foo(n):
    """(A part of this Problem is to be able to figure out what this
    function is doing. Therefore, no details are provided in
    the docstring.)
    """
    my_list = []
    for i in range(n):
        num = np.random.randint(-9,9)
        my_list.append(num)
    evens = 0
    for j in range(n):
        if j%2 == 0:
            evens += my_list[j]
    return my_list, evens

# Problem 5
def foo_opt(n):
    """An optimized version of 'foo'"""
    my_list = list(np.random.randint(-9,9,n))
    evens = sum(x for x in my_list if not x%2)
    return my_list, evens

def compare_foo(n):
    """Prints a comparison of foo and foo_opt"""
    print compare_timings(foo, foo_opt, n)
#compare_foo(10000)

# Problem 6
def pymatpow(X, power):
    """ Return X^{power}.

    Inputs:
        X (array) - A square 2-D NumPy array
        power (int) - The power to which we are taking the matrix X.
    Returns:
        prod (array) - X^{power}
    """
    prod = X.copy()
    temparr = np.empty_like(X[0])
    size = X.shape[0]
    for n in xrange(1, power):
        for i in xrange(size):
            for j in xrange(size):
                tot = 0.
                for k in xrange(size):
                    tot += prod[i,k] * X[k,j]
                temparr[j] = tot
            prod[i] = temparr
    return prod

@jit
def numba_matpow(X, power):
    """ Return X^{power}. Compiled using Numba.

    Inputs:
        X (array) - A square 2-D NumPy array
        power (int) - The power to which we are taking the matrix X.
    Returns:
        prod (array) - X^{power}
    """
    prod = X.copy()
    temparr = np.empty_like(X[0])
    size = X.shape[0]
    for n in xrange(1, power):
        for i in xrange(size):
            for j in xrange(size):
                tot = 0.
                for k in xrange(size):
                    tot += prod[i,k] * X[k,j]
                temparr[j] = tot
            prod[i] = temparr
    return prod

def numpy_matpow(X, power):
    """ Return X^{power}.

    Inputs:
        X (array) - A square 2-D NumPy array
        power (int) - The power to which we are taking the matrix X.
    Returns:
        prod (array) - X^{power}
    """
    prod = X.copy()
    for n in xrange(1,power):
        prod = np.dot(prod, X)
    return prod


def compare_matpow(X, power):
    """Prints a comparison of pymatpow and numba_matpow and prints a
    comparison of pymatpow and numpy_matpow"""
    numba_matpow(X, power)
    print compare_timings(pymatpow, numba_matpow, X, power)
    print compare_timings(pymatpow, numpy_matpow, X, power)

def test6():
    X = np.random.randint(10, size=(2,2))
    compare_matpow(X, 10)
#test6()

# Problem 7
def pytridiag(a,b,c,d):
    """Solve the tridiagonal system Ax = d where A has diagonals a, b, and c.

    Inputs:
        a, b, c, d (array) - All 1-D NumPy arrays of equal length.
    Returns:
        x (array) - solution to the tridiagonal system.
    """
    n = len(a)

    # Make copies so the original arrays remain unchanged
    aa = np.copy(a)
    bb = np.copy(b)
    cc = np.copy(c)
    dd = np.copy(d)

    # Forward sweep
    for i in xrange(1, n):
        temp = aa[i]/bb[i-1]
        bb[i] = bb[i] - temp*cc[i-1]
        dd[i] = dd[i] - temp*dd[i-1]

    # Back substitution
    x = np.zeros_like(a)
    x[-1] = dd[-1]/bb[-1]
    for i in xrange(n-2, -1, -1):
        x[i] = (dd[i]-cc[i]*x[i+1])/bb[i]

    return x

def init_tridiag(n):
    """Initializes a random nxn tridiagonal matrix A.

    Inputs:
        n (int) : size of array

    Returns:
        a (1-D array) : (-1)-th diagonal of A
        b (1-D array) : main diagonal of A
        c (1-D array) : (1)-th diagonal of A
        A (2-D array) : nxn tridiagonal matrix defined by a,b,c.
    """
    a = np.random.random_integers(1,9,n).astype("float")
    b = np.random.random_integers(1,9,n).astype("float")
    c = np.random.random_integers(1,9,n).astype("float")

    A = np.zeros((b.size,b.size))
    np.fill_diagonal(A,b)
    np.fill_diagonal(A[1:,:-1],a[1:])
    np.fill_diagonal(A[:-1,1:],c)
    return a,b,c,A

@jit
def numba_tridiag(a,b,c,d):
    """Solve the tridiagonal system Ax = d where A has diagonals a, b, and c.

    Inputs:
        a, b, c, d (array) - All 1-D NumPy arrays of equal length.
    Returns:
        x (array) - solution to the tridiagonal system.
    """
    n = len(a)

    # Make copies so the original arrays remain unchanged
    aa = np.copy(a)
    bb = np.copy(b)
    cc = np.copy(c)
    dd = np.copy(d)

    # Forward sweep
    for i in xrange(1, n):
        temp = aa[i]/bb[i-1]
        bb[i] = bb[i] - temp*cc[i-1]
        dd[i] = dd[i] - temp*dd[i-1]

    # Back substitution
    x = np.zeros_like(a)
    x[-1] = dd[-1]/bb[-1]
    for i in xrange(n-2, -1, -1):
        x[i] = (dd[i]-cc[i]*x[i+1])/bb[i]

    return x

def scipy_tridiag(a,b,c,d):
    A = np.zeros((b.size,b.size))
    np.fill_diagonal(A,b)
    np.fill_diagonal(A[1:,:-1],a[1:])
    np.fill_diagonal(A[:-1,1:],c)
    
    return linalg.solve(A, d)

def compare_tridiag():
    """Prints a comparison of numba_tridiag and pytridiag and prints
    a comparison of numba_tridiag and scipy.linalg.solve."""
    a, b, c, A = init_tridiag(1000000)
    d = np.random.randint(1,9,1000000)
    numba_tridiag(a, b, c, d)
    print compare_timings(pytridiag, numba_tridiag, a, b, c, d)
    a2, b2, c2, A2 = init_tridiag(1000)
    d2 = np.random.randint(1,9,1000)
    print compare_timings(pytridiag, scipy_tridiag, a2, b2, c2, d2)
#compare_tridiag()

# Problem 8
def householder(A):
    '''
    Use the Householder algorithm to compute the QR decomposition
    of a matrix.
    Accept an m by n matrix A of rank n. 
    Return Q, R
    '''
    (m,n) = np.shape(A)
    R = np.copy(A)
    Q = np.identity(m)
    for k in range(n-1):
        uk = np.copy(R[k:,k])
        uk[0] = uk[0]+(np.sign(uk[0])*np.linalg.norm(uk))
        uk = uk/np.linalg.norm(uk)
        for l in range(k,n-1):
            R[k:,k:] = R[k:,k:] - 2*np.outer(uk, np.dot(uk.T, R[k:,k:]))
            Q[k:] = Q[k:] - 2*np.outer(uk, np.dot(uk.T, Q[k:]))
    QH = Q.T.conjugate()
    return QH, R


def opt_householder(A):
    '''
    Use the Householder algorithm to compute the QR decomposition
    of a matrix.
    Accept an m by n matrix A of rank n. 
    Return Q, R
    '''
    (m,n) = np.shape(A)
    R = np.copy(A)
    Q = np.identity(m)
    for k in xrange(n-1):
        uk = np.copy(R[k:,k])
        uk[0] = uk[0]+(np.sign(uk[0])*np.linalg.norm(uk))
        uk = uk/np.linalg.norm(uk)
        R[k:,k:] = R[k:,k:] - 2*np.outer(uk, np.dot(uk.T, R[k:,k:]))
        Q[k:] = Q[k:] - 2*np.outer(uk, np.dot(uk.T, Q[k:]))
    QH = Q.T.conjugate()
    return QH, R



def compare_old():
    """Prints a comparison of an old algorithm from a previous lab
    and the optimized version."""
    A = np.random.randint(1,9, size=(250,250))
    print compare_timings(householder, opt_householder, A)
#compare_old()

"""
I took out one of the loops that was previously in the householder function and array slicing instead whoich is incredibly faster. I also changed all of the range() calls to xrange() the time went from 11 seconds to .08 on my machine.  
"""
