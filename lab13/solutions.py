import numpy as np
from sympy import subfactorial
from numpy import linalg as la
from numpy.random import normal
from matplotlib import pyplot as plt

##############  Problem 1  ##############
def prob1():
    '''
    Randomly perturb w_coeff by replacing each coefficient a_i with a_i*r_i, where
    r_i is drawn from a normal distribution centered at 1 with varience 1e-10.
    	
    Plot the roots of 100 such experiments in a single graphic, along with the roots
    of the unperturbed polynomial w(x)
    	
    Using the final experiment only, estimate the relative and absolute condition number
    (in any norm you prefer).
    	
    RETURN:
    Should display graph of all 100 perturbations.
    Should print values of relative and absolute condition.
    '''
    w_roots = np.arange(1, 21)
    w_coeffs = np.array([1, -210, 20615, -1256850, 53327946, -1672280820,
				40171771630, -756111184500, 11310276995381,
    				-135585182899530, 1307535010540395,
    				-10142299865511450, 63030812099294896,
    				-311333643161390640, 1206647803780373360,
    				-3599979517947607200, 8037811822645051776,
    				-12870931245150988800, 13803759753640704000,
    				-8752948036761600000, 2432902008176640000])
    mu = 1
    sigma = 1e-10
    for i in xrange(100):
        perturb = normal(mu, sigma, len(w_coeffs))
        perturbed_coeffs = w_coeffs * perturb
        perturbed_roots = np.roots(np.poly1d(perturbed_coeffs))
        plt.scatter(np.real(perturbed_roots), np.imag(perturbed_roots),s=.5, c = 'k')
        absolute = la.norm(perturbed_roots-w_roots, np.inf)/la.norm(perturb, np.inf)
        relative = absolute*la.norm(w_coeffs, np.inf)/la.norm(w_roots, np.inf)
    plt.scatter(np.real(w_roots), np.imag(w_roots))
    plt.show()
    
    print("Absolute Condition Number: {}".format(str(absolute)))
    print("Relative Condition Number: {}".format(str(relative)))

#prob1()
    				


##############  Problem 2  ##############	
def eig_condit(M):
    '''
    Approximate the condition number of the eigenvalue problem at M.
    
    INPUT:
    M - A 2-D square NumPy array, representing a square matrix.
    
    RETURN:
    A tuple containing approximations to the absolute and 
    relative condition numbers of the eigenvalue problem at M.
    '''
    mu = 0
    sigma = 1e-10
    perturb = normal(mu, sigma, M.shape) + normal(mu, sigma, M.shape)*1j
    eigs = la.eig(M)[0]
    eigsp = la.eig(M+perturb)[0]
    absolute = la.norm(eigs-eigsp)/la.norm(perturb)
    relative = absolute*la.norm(M)/la.norm(eigs)
    return (absolute, relative)

def prob2():
    M = np.array([[420,2012498723],[1,20012340]])
    print eig_condit(M)
    N = np.array([[1,2],[2,1]])
    print eig_condit(N)
#prob2()

#   1 pt extra credit
def plot_eig_condit(x0=-100, x1=100, y0=-100, y1=100, res=10):
    '''
    Create a grid of points. For each pair (x,y) in the grid, find the 
    relative condition number of the eigenvalue problem, using the matrix 
    [[1 x]
     [y 1]]
    as your input. You can use plt.pcolormesh to plot the condition number
    over the entire grid.
    
    INPUT:
    x0 - min x-value of the grid
    x1 - max x-value
    y0 - min y-value
    y1 - max y-value
    res - number of points along each edge of the grid
    '''
    raise NotImplementedError('plot_eig_condit not implemented')

##############  Problem 3  ##############
def integral(n):
    '''
    RETURN I(n)
    '''
    return (-1)**n * subfactorial(n) + (-1)**(n+1) * (np.math.factorial(n))/(np.e)

def prob3():
    '''
    For the values of n in the problem, compute integral(n). Compare
    the values to the actual values, and print your explanation of what
    is happening.
    '''
    n_values = np.array([1,5,10,15,20,25,30,35,40,45,50])
    
    #actual values of the integral at specified n
    actual_values = [0.367879441171, 0.145532940573, 0.0838770701034, 
                 0.0590175408793, 0.0455448840758, 0.0370862144237, 
                 0.0312796739322, 0.0270462894091, 0.023822728669, 
                 0.0212860390856, 0.0192377544343] 
    approx = np.zeros(len(actual_values))
    for idx, n in enumerate(n_values):
        approx[idx] = integral(n)
    print("The errors (the actual value minus the approximation) are as follows:")
    print (actual_values - approx)
    """
    The difference values vary so much because this is an unstable approximation. 
    """
#prob3()
