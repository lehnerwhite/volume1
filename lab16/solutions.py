# spec.py
"""Volume 1, Lab 16: Importance Sampling and Monte Carlo Simulations.
Lehner White
Math 347
Jan 27, 2015
"""
import numpy as np
from scipy.stats import gamma
from scipy.stats import norm
from scipy.stats import multivariate_normal as mnorm
from matplotlib import pyplot as plt

# Problem 1 
def prob1(n):
    """Approximate the probability that a random draw from the standard
    normal distribution will be greater than 3.
    Returns: your estimated probability.
    """
    sample = np.random.randn(n)
    return np.sum(sample>3)/float(n)

# Problem 2
def prob2():
    """Answer the following question using importance sampling: 
            A tech support hotline receives an average of 2 calls per 
            minute. What is the probability that they will have to wait 
            at least 10 minutes to receive 9 calls?
    Returns:
        IS (array) - an array of estimates using 
            [5000, 10000, 15000, ..., 500000] as number of 
            sample points."""
    h = lambda x : x > 10
    f = lambda x : gamma(a = 9,scale=1/2.).pdf(x)
    g = lambda x : norm(loc=11,scale=1).pdf(x)
    
    N = np.arange(5000, 500000, 5000)
    MC_estimates = np.zeros(len(N))

    for idx, num in enumerate(N):
        X = np.random.normal(11., scale = 1., size = num)
        MC = 1./num * np.sum(h(X)*f(X)/g(X))
        MC_estimates[idx] = MC

    return MC_estimates

# Problem 3
def prob3():
    """Plot the errors of Monte Carlo Simulation vs Importance Sampling
    for the prob2()."""
    h = lambda x : x > 10
    f = lambda x : gamma(a = 9,scale=1/2.).pdf(x)
    g = lambda x : norm(loc=11,scale=1).pdf(x)
    
    N = np.arange(5000, 505000, 5000)
    IS_estimates = np.zeros(len(N))

    for idx, num in enumerate(N):
        X = np.random.normal(11., scale = 1., size = num)
        MC = 1./num * np.sum(h(X)*f(X)/g(X))
        IS_estimates[idx] = MC

    MC_estimates = []
    for N in xrange(5000,505000,5000):
        X = np.random.gamma(9,scale=0.5,size=N)
        MC = 1./N*np.sum(h(X))
        MC_estimates.append(MC)
    MC_estimates = np.array(MC_estimates)
    
    actual_prob = 1 - gamma(a=9,scale=0.5).cdf(10)
    
    plt.plot(np.arange(5000,505000, 5000), MC_estimates-actual_prob, label='Monte Carlo', color='r')
    plt.plot(np.arange(5000,505000, 5000), IS_estimates-actual_prob, label='Importance Sampling', color='b')
    plt.ylim(ymin=-0.00001)
    plt.title('Error of Estimations')
    plt.xlabel('Number of Sample Points')
    plt.legend()
    plt.show()

# Problem 4
def prob4():
    """Approximate the probability that a random draw from the
    multivariate standard normal distribution will be less than -1 in 
    the x-direction and greater than 1 in the y-direction.
    Returns: your estimated probability"""
    h = lambda x : x[0] < -1 and x[1] > 1
    f = lambda x : mnorm(mean=np.array([0,0])).pdf(x)
    g = lambda x : mnorm(mean=np.array([-1,1])).pdf(x)

    N = 50000
    
    X = np.random.normal(loc=-1, scale=1,size=N)
    Y = np.random.normal(loc=1, scale=1,size=N)
    XY = np.vstack((X,Y)).T
    
    prob = 0.
    for idx in xrange(N):
        prob += 1./N * np.sum(h(XY[idx])*f(XY[idx])/g(XY[idx]))
    
    return prob
