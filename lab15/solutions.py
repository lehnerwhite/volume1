# spec.py
"""Volume I: Monte Carlo Integration
<Name>
<Class>
<Date>
"""
import numpy as np

# Problem 1
def prob1(numPoints=10e5):
    """Return an estimate of the volume of the unit sphere using Monte
    Carlo Integration.
    
    Inputs:
        numPoints (int, optional) - Number of points to sample. Defaults
            to 10^5.
    Returns:
        volume (int) - Approximate value of the area of the unit sphere.
    """
    points = np.random.rand(3, numPoints)
    points = points*2 - 1
    circleMask = np.sqrt(points[0,:]**2 + points[1,:]**2 + points[2,:]**2) <= 1
    numInCircle = np.count_nonzero(circleMask)
    volume = 8.*numInCircle/numPoints
    return volume

#print prob1()

# Problem 2
def prob2(numPoints=10e5):
    """Return an estimate of the area under the curve,
    f(x) = |sin(10x)cos(10x) + sqrt(x)*sin(3x)| from 1 to 5.
    
    Inputs:
        numPoints (int, optional) - Number of points to sample. Defautls
            to 10^5.
    Returns:
        area (int) - Apprimate value of the area under the 
            specified curve.
    """
    points = np.random.rand(numPoints)
    points = points*4. + 1
    values = np.abs(np.sin(10.*points)*np.cos(10*points) + np.sqrt(points)*np.sin(3*points))
    approx = 4*np.sum(values)/len(values)
    return approx

#print prob2()


# Problem 3
def mc_int(f, mins, maxs, numPoints=500, numIters=100):
    """Use Monte-Carlo integration to approximate the integral of f
    on the box defined by mins and maxs.
    
    Inputs:
        f (function) - The function to integrate. This function should 
            accept a 1-D NumPy array as input.
        mins (1-D np.ndarray) - Minimum bounds on integration.
        maxs (1-D np.ndarray) - Maximum bounds on integration.
        numPoints (int, optional) - The number of points to sample in 
            the Monte-Carlo method. Defaults to 500.
        numIters (int, optional) - An integer specifying the number of 
            times to run the Monte Carlo algorithm. Defaults to 100.
        
    Returns:
        estimate (int) - The average of 'numIters' runs of the 
            Monte-Carlo algorithm.
                
    Example:
        >>> f = lambda x: np.hypot(x[0], x[1]) <= 1
        >>> # Integral over the square [-1,1] x [-1,1]. Should be pi.
        >>> mc_int(f, np.array([-1,-1]), np.array([1,1]))
        3.1290400000000007
    """
    n = len(mins)
    stretch = maxs - mins
    total = 0.
    for i in xrange(numIters):
        points = np.random.rand(numPoints, n)
        points = points*stretch + mins
        values = np.apply_along_axis(f,1,points)
        approx = np.prod(stretch)*np.sum(values)/float(len(values))
        total += approx
    integral = total / numIters
    return integral

#f = lambda x: np.hypot(x[0], x[1]) <= 1
#f2 = lambda x: np.abs(np.sin(10.*x)*np.cos(10*x) + np.sqrt(x)*np.sin(3*x))
#print mc_int(f, np.array([-3,-3]), np.array([3,3]), numPoints = 10000)


# Problem 4
def prob4(numPoints=[500]):
    """Calculates an estimate of the integral of 
    f(x,y,z,w) = sin(x)y^5 - y^5 + zw + yz^3
    
    Inputs:
        numPoints (list, optional) - a list of the number of points to 
            use in the approximation. Defaults to [500].
    Returns:
        errors (list) - a list of the errors when calculating the 
            approximation using 'numPoints' points.
    Example:
    >>> prob4([100,200,300])
    [-0.061492011289160729, 0.016174426377108819, -0.0014292910207835802]
    """
    f = lambda x: np.sin(x[0])*x[1]**5 - x[1]**5 + x[2]*x[3] + x[1]*x[2]**3
    maxs = np.array([1,1,1,1])
    mins = np.array([-1,-1,-1,-1])
    for num in numPoints:
        approx = np.abs(mc_int(f, mins, maxs, numPoints=num))
        print("For {} points the error of the Monte-Carlo approximation had an error of {}".format(str(num), str(approx)))
#prob4([100,1000,10000])

