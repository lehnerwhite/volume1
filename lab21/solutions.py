import csv
import numpy as np
import scipy.sparse as spar
import scipy.linalg as la
from scipy.sparse import linalg as sla

def to_matrix(filename,n):
    '''
    Return the nxn adjacency matrix described by datafile.
    INPUTS:
    datafile (.txt file): Name of a .txt file describing a directed graph. 
        Lines describing edges should have the form '<from node>\t<to node>\n'.
        The file may also include comments.
    n (int): The number of nodes in the graph described by datafile
    RETURN:
        Return a SciPy sparse `dok_matrix'.
    '''
    adj = spar.dok_matrix((n,n), dtype =np.float32)
    with open(filename, 'r') as data:
        for line in data:
            line = line.strip().split()
            try:
                adj[int(line[0]),int(line[1])] = 1 
            except IndexError:
                pass
            except TypeError:
                pass
            except ValueError:
                pass
    return adj

def calculateK(A,N):
    '''
    Compute the matrix K as described in the lab.
    Input:
        A (array): adjacency matrix of an array
        N (int): the datasize of the array
    Return:
        K (array)
    '''
    D = np.sum(A, axis = 1)
    sinks = np.where(D==0)
    for idx in sinks[0]:
        A[idx, :] = 1
    D = np.sum(A, axis = 1)
    K = np.divide(A, D).T
    return K

def test12():
    A = to_matrix('datafile.txt', 8).todense()
    print A
    print calculateK(A, A.shape[0])

def iter_solve(adj, N=None, d=.85, tol=1E-5):
    '''
    Return the page ranks of the network described by `adj`.
    Iterate through the PageRank algorithm until the error is less than `tol'.
    Inputs:
    adj - A NumPy array representing the adjacency matrix of a directed graph
    N (int) - Restrict the computation to the first `N` nodes of the graph.
            Defaults to N=None; in this case, the entire matrix is used.
    d     - The damping factor, a float between 0 and 1.
            Defaults to .85.
    tol  - Stop iterating when the change in approximations to the solution is
        less than `tol'. Defaults to 1E-5.
    Returns:
    The approximation to the steady state.
    '''
    if N is not None:
        adj = adj[:N, :N]
    elif N is None:
        N = adj.shape[0]
    p0 = 1./N * np.ones((N,1))
    diff = np.inf
    while diff > tol:
        p = d * np.dot(calculateK(adj, N), p0) + (1.-d)/N * np.ones((N,1))
        diff = np.linalg.norm(p0 - p)
        p0 = p 
    return p

def test3():
    A = to_matrix('datafile.txt', 8).todense()
    print iter_solve(A)

def eig_solve( adj, N=None, d=.85):
    '''
    Return the page ranks of the network described by `adj`. Use the
    eigenvalue solver in scipy.linalg to calculate the steady state
    of the PageRank algorithm
    Inputs:
    adj - A NumPy array representing the adjacency matrix of a directed graph
    N - Restrict the computation to the first `N` nodes of the graph.
            Defaults to N=None; in this case, the entire matrix is used.
    d     - The damping factor, a float between 0 and 1.
            Defaults to .85.
    Returns:
    The approximation to the steady state.
    '''
    if N is not None:
        adj= adj[:N, :N]
    elif N is None:
        N = adj.shape[0]

    B = d * calculateK(adj, N) + (1.-d)/N * np.ones((N,N))
    vec =  np.real(la.eig(B)[1][:,0])
    ranks = vec / np.sum(vec)
    return ranks

def test4():
    A = to_matrix('datafile.txt', 8).todense()
    print eig_solve(A, N=8)

def problem5(filename='ncaa2013.csv'):
    '''
    Create an adjacency matrix from the input file.
    Using iter_solve with d = 0.7, run the PageRank algorithm on the adjacency 
    matrix to estimate the rankings of the teams.
    Inputs:
    filename - Name of a .txt file containing data for basketball games. 
        Should contain a header row: 'Winning team,Losing team",
        after which each row contains the names of two teams,
        winning and losing, separated by a comma
    Returns:
    sorted_ranks - The array of ranks output by iter_solve, sorted from highest
        to lowest.
    sorted_teams - List of team names, sorted from highest rank to lowest rank.   
    '''
    data = []
    with open(filename, 'rb') as csvfile:
        reader = csv.reader(csvfile, delimiter = ',')
        next(reader, None)
        for line in reader:
            data.append((line[0], line[1]))
    
    team_set = set()
    
    for tup in data:
        team_set.add(tup[0])
        team_set.add(tup[1])

    teams = list(team_set)
    teams = sorted(teams)
    n = len(teams) 
    
    adj = np.zeros((n,n))
    for tup in data:
        adj[teams.index(tup[1]), teams.index(tup[0])] = 1 
    
    ranks = iter_solve(adj, d=0.7)
    #sorted_team_idx = np.argsort(ranks.flatten())[::-1]
    #sorted_teams = [teams[i] for i in sorted_team_idx]
    sorted_teams = [sorted_team for (sorted_rank, sorted_team) in sorted(zip(list(ranks), teams))][::-1]
    sorted_ranks = np.sort(ranks)[::-1]
    return sorted_ranks, sorted_teams
#print problem5('ACME_VIM/volume1/lab21/ncaa2013.csv')[1] 

def problem6():
    '''
    Optional problem: Load in and explore any one of the SNAP datasets.
    Run the PageRank algorithm on the adjacency matrix.
    If you can, create sparse versions of your algorithms from the previous
    problems so that they can handle more nodes.
    '''
    pass
            
