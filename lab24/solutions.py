import numpy as np
from scipy import linalg as la
from matplotlib import pyplot as plt

# Problem 1
def ps_scatter_plot(A, epsilon=.001, num_pts=20):
    '''Plots the 'poorman's pseudospectrum' of a matrix A
   
    Parameters:
    -----------
    A : ndarray of size (n,n)
        The matrix whose pseudospectrum is to be plotted.
    epsilon : float
        The norm of the random matrices that are generated.
        Defaults to 10**-3
    num_pts : int
        The number of matrices, E, that will be used in the
        algorithm. Defaults to 20.
    '''
    m,n = A.shape
    A_eigs, vecs = la.eig(A)
    for i in xrange(num_pts):
        E = np.random.rand(m,n)
        E = E * epsilon / la.norm(E)
        AE = A + E
        AE_eigs, vecs = la.eig(AE)
        plt.scatter(AE_eigs.real, AE_eigs.imag)
    plt.scatter(A_eigs.real, A_eigs.imag, color='r')
    plt.show()

def test1():
    a0 = np.zeros((1,120))
    a1 = np.ones((1,119)) * 1j
    a2 = np.ones((1,119)) * -1j
    a3 = np.ones((1,118)) * 1
    a4 = np.ones((1,118)) * -1
    A = np.diagflat(a0) + np.diagflat(a1, k=1) + np.diagflat(a4, k=2) + np.diagflat(a2, k=-1) + np.diagflat(a3, k=-2)
    print A.shape
    ps_scatter_plot(A)
#test1()


# Problem 2
def ps_contour_plot(A, m = 20,epsilon_vals=None):
    '''Plots the pseudospectrum of the matrix A as a contour plot.  Also,
    plots the eigenvalues.
    Parameters:
        A : square, 2D ndarray
            The matrix whose pseudospectrum is to be plotted
        m : int
            accuracy
        epsilon_vals : list of floats
            If k is in epsilon_vals, then the epsilon-pseudospectrum
            is plotted for epsilon=10**-k
            If epsilon_vals=None, the defaults of plt.contour() are used
            instead of any specified values.
    '''
    n,n = A.shape
    T = la.schur(A)[0]
    eigsA = np.diag(T)
    xvals, yvals = ps_grid(eigsA, m)
    sigmin = np.zeros((m,m))
    for k in xrange(m):
        for j in xrange(m):
            T1 = (xvals[k] + 1j * yvals[j]) * np.eye(n) - T
            T2 = np.conjugate(T1).T
            sigold = 0
            qold = np.zeros((n,1))
            beta = 0
            H = np.zeros((n,n))
            q = np.random.rand(n,1) + np.random.rand(n,1)*1j 
            q = (q) / np.linalg.norm(q)
            for p in xrange(n-1):
                b1 = np.linalg.solve(T2, q)
                b2 = np.linalg.solve(T1, b1)
                v = b2 - beta * qold
                alpha = np.real(np.vdot(q, v))
                v = v - alpha *q
                beta = np.linalg.norm(v)
                qold = q
                q = v / beta
                H[p+1,p] = beta
                H[p,p+1] = beta
                H[p,p] = alpha
                sig = np.abs(np.amax(np.linalg.eig(H[:p+1, :p+1])[0]))
                if (np.abs(sigold/sig - 1) < 0.001):
                    break
                sigold = sig
            sigmin[j,k] = np.sqrt(sig)
    
    plt.contour(xvals,yvals,np.log10(sigmin), levels=epsilon_vals)
    A_eigs, vecs = np.linalg.eig(A)
    plt.scatter(A_eigs.real, A_eigs.imag)
    plt.show()

def ps_grid(eig_vals, grid_dim):
    """
        Computes the grid on which to plot the pseudospectrum
        of a matrix. This is a helper function for ps_contour_plot().
        """
    x0, x1 = min(eig_vals.real), max(eig_vals.real)
    y0, y1 = min(eig_vals.imag), max(eig_vals.imag)
    xmid = (x0 + x1) /2.
    xlen1 = x1 - x0 +.01
    ymid = (y0 + y1) / 2.
    ylen1 = y1 - y0 + .01
    xlen = max(xlen1, ylen1/2.)
    ylen = max(xlen1/2., ylen1)
    x0 = xmid - xlen
    x1 = xmid + xlen
    y0 = ymid - ylen
    y1 = ymid + ylen
    x = np.linspace(x0, x1, grid_dim)
    y = np.linspace(y0, y1, grid_dim)
    return x,y

def test2():
    a0 = np.zeros((1,120))
    a1 = np.ones((1,119)) * 1j
    a2 = np.ones((1,119)) * -1j
    a3 = np.ones((1,118)) * 1
    a4 = np.ones((1,118)) * -1
    A = np.diagflat(a0) + np.diagflat(a1, k=1) + np.diagflat(a4, k=2) + np.diagflat(a2, k=-1) + np.diagflat(a3, k=-2)
    ps_contour_plot(A, m=50)
#test2()



def problem3(n=120,epsilon=.001,num_pts=20):
    '''
    Parameters:
    n : int
    The size of the matrix to use. Defaults to a 120x120 matrix.
    epsilon : float
    The norm of the random matrices that are generated.
    Defaults to 10**-3
    num_pts : int
    The number of matrices, E, that will be used in the
    algorithm. Defaults to 20.
    '''
    a0 = np.zeros((1,n))
    a1 = np.ones((1,n-1)) * 1j
    a2 = np.ones((1,n-1)) * -1j
    a3 = np.ones((1,n-2)) * 1
    a4 = np.ones((1,n-2)) * -1
    a5 = np.random.rand(1,n) + np.random.rand(1,n)*1j
    A1 = np.diagflat(a0) + np.diagflat(a1, k=1) + np.diagflat(a4, k=2) + np.diagflat(a2, k=-1) + np.diagflat(a3, k=-2)
    A2 = np.diagflat(a0) + np.diagflat(a1, k=1) + np.diagflat(a3, k=2) + np.diagflat(a2, k=-1) + np.diagflat(a3, k=-2)
    A3 = np.diagflat(a5) + np.diagflat(a1, k=1) + np.diagflat(a3, k=2)
    ps_contour_plot(A1, 20)
    ps_contour_plot(A2, 20)
    ps_contour_plot(A3, 20)

#problem3()


