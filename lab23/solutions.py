import numpy as np
from matplotlib import pyplot as plt

def gmres(A, b, x0, k=100, tol=1e-8):
    '''Calculate approximate solution of Ax=b using GMRES algorithm.
        
    INPUTS:
    A    - Callable function that calculates Ax for any input vector x.
    b    - A NumPy array of length m.
    x0   - An arbitrary initial guess.
    k    - Maximum number of iterations of the GMRES algorithm. Defaults to 100.
    tol  - Stop iterating if the residual is less than 'tol'. Defaults to 1e-8.
    
    RETURN:
    Return (y, res) where 'y' is an approximate solution to Ax=b and 'res'
    is the residual.
    
    Examples:
    >>> a = np.array([[1,0,0],[0,2,0],[0,0,3]])
    >>> A = lambda x: a.dot(x)
    >>> b = np.array([1, 4, 6])
    >>> x0 = np.zeros(b.size)
    >>> gmres(A, b, x0)
    (array([ 1.,  2.,  2.]), 1.09808907533e-16)
    '''
    Q = np.empty((b.size, k+1))
    H = np.zeros((k+1, k))
    r0 = b - A(x0)
    beta = np.linalg.norm(r0)
    Q[:, 0] = r0 / beta
    e = np.zeros(k+1)
    e[0] = 1.
    for n in xrange(k):
        Q[:,n+1] = A(Q[:,n])
        for i in xrange(n+1):
            H[i,n] = np.dot(Q[:,i].conjugate().T, Q[:,n+1])
            Q[:,n+1] = Q[:,n+1] - H[i,n] * Q[:,i]
        H[n+1,n] = np.sqrt(np.vdot(Q[:,n+1],Q[:,n+1].conjugate()))
        Q[:,n+1] = Q[:,n+1] / H[n+1,n]
        yn, res, rank, s = np.linalg.lstsq(H[:n+2,:n+1], beta*e[:n+2])
        res = np.sqrt(res)
        if res < tol:
            return np.dot(Q[:,:n+1], yn[:n+1]) + x0, res
    return np.dot(Q[:,:k+1], yn[:k+1]) + x0, res

def test1():
    a = np.array([[1,0,0],[0,2,0],[0,0,3]])
    A = lambda x: a.dot(x)
    b = np.array([1, 4, 6])
    x0 = np.zeros(b.size)
    print gmres(A, b, x0)

#test1()

def plot_gmres(A, b, x0, tol=1e-8):
    '''Use the GMRES algorithm to approximate the solution to Ax=b. 
        Plot the eigenvalues of A and the convergence of the algorithm.
    
    INPUTS:
    A   - A 2-D NumPy array of shape mxm.
    b   - A 1-D NumPy array of length m.
    x0  - An arbitrary initial guess.
    tol - Stop iterating and create the desired plots when the residual is
    less than 'tol'. Defaults to 1e-8.
    
    OUTPUT:
    Follow the GMRES algorithm until the residual is less than tol, for a
    maximum of m iterations. Then create the two following plots (subplots
    of a single figure):
    
    1. Plot the eigenvalues of A in the complex plane.
    
    2. Plot the convergence of the GMRES algorithm by plotting the
    iteration number on the x-axis and the residual on the y-axis.
    Use a log scale on the y-axis.
    '''
    A1 = np.copy(A)
    A = A.dot
    totRes = []
    k = b.size
    Q = np.empty((b.size, k+1))
    H = np.zeros((k+1, k))
    r0 = b - A(x0)
    beta = np.linalg.norm(r0)
    Q[:, 0] = r0 / beta
    e = np.zeros(k+1)
    e[0] = 1.
    
    n = 0
    res = np.inf
    
    while n < k and res >= tol:
        Q[:,n+1] = A(Q[:,n])
        for i in xrange(n+1):
            H[i,n] = np.dot(Q[:,i].conjugate().T, Q[:,n+1])
            Q[:,n+1] = Q[:,n+1] - H[i,n] * Q[:,i]
        H[n+1,n] = np.sqrt(np.vdot(Q[:,n+1],Q[:,n+1].conjugate()))
        Q[:,n+1] = Q[:,n+1] / H[n+1,n]
        yn, res, rank, s = np.linalg.lstsq(H[:n+2,:n+1], beta*e[:n+2])
        res = np.sqrt(res)
        totRes.append(res)
        n += 1
    
    evals, evecs = np.linalg.eig(A1)
    Re = np.real(evals)
    Im = np.imag(evals)
    
    plt.subplot(121)
    plt.scatter(Re, Im)
    plt.subplot(122)
    plt.yscale('log')
    plt.plot(np.arange(n), totRes)
    plt.show()

def problem2():
    '''Create the function for problem 2 which calls plot_gmres on An for n = -4,-2,0,2,4.
        Print out an explanation of how the convergence of the GMRES algorithm
        relates to the eigenvalues.
        
    '''
    a = np.random.normal(loc = 0, scale = 1/(np.sqrt(200)), size = (200,200))
    b = np.ones(200)
    x0 = np.zeros(b.size)
    N = [-4,-2,0,2,4]
    
    for n in N:
        A = n * np.eye(200) + a
        plot_gmres(a, b, x0)

#problem2()

def gmres_k(A, b, x0, k=5, tol=1E-8, restarts=50):
    '''Use the GMRES(k) algorithm to approximate the solution to Ax=b.
        
    INPUTS:
    A        - A callable function that calculates Ax for any vector x.
    b        - A NumPy array.
    x0       - An arbitrary initial guess.
    k        - Maximum number of iterations of the GMRES algorithm before
    restarting. Defaults to 5.
    tol      - Stop iterating if the residual is less than 'tol'. Defaults
    to 1E-8.
    restarts - Maximum number of restarts. Defaults to 50.
    
    RETURN:
    Return (y, res) where 'y' is an approximate solution to Ax=b and 'res'
    is the residual.
    '''
    for l in xrange(restarts):
        Q = np.empty((b.size, k+1))
        H = np.zeros((k+1, k))
        r0 = b - A(x0)
        beta = np.linalg.norm(r0)
        Q[:, 0] = r0 / beta
        e = np.zeros(k+1)
        e[0] = 1.
        for n in xrange(k):
            Q[:,n+1] = A(Q[:,n])
            for i in xrange(n+1):
                H[i,n] = np.dot(Q[:,i].conjugate().T, Q[:,n+1])
                Q[:,n+1] = Q[:,n+1] - H[i,n] * Q[:,i]
            H[n+1,n] = np.sqrt(np.vdot(Q[:,n+1],Q[:,n+1].conjugate()))
            Q[:,n+1] = Q[:,n+1] / H[n+1,n]
            yn, res, rank, s = np.linalg.lstsq(H[:n+2,:n+1], beta*e[:n+2])
            res = np.sqrt(res)
            if res < tol:
                return np.dot(Q[:,:n+1], yn[:n+1]) + x0, res
        x0 += np.dot(Q[:,:k], yn[:n+1])
    return np.dot(Q[:,:k+1], yn[:k+1]) + x0, res

def test3():
    a = np.array([[1,0,0],[0,2,0],[0,0,3]])
    A = lambda x: a.dot(x)
    b = np.array([1, 4, 6])
    x0 = np.zeros(b.size)    
    problem2()
    gmres_k(A, b, x0)
        
#test3()

