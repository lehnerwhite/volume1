04/01/16 12:20 

Problem 1 (20 points):
shape of H is incorrect; should be (6, 6) for dim=7, k=6
ValueError: operands could not be broadcast together with shapes (6,6) (6,7) 

Problem 2 (15 points):
Score += 15

Problem 3 (10 points):
AttributeError: 'NoneType' object has no attribute 'rint'

Problem 4 (5 points):
NotImplementedError: plot_ritz not implemented

Total score: 15/50 = 30.0%


Comments:
	Make problem 3 return the answer instead of printing

-------------------------------------------------------


04/06/16 12:50 

Problem 1 (20 points):
Score += 20

Problem 2 (15 points):
Score += 15

Problem 3 (10 points):
Score += 10

Problem 4 (5 points):
NotImplementedError: plot_ritz not implemented

Total score: 45/50 = 90.0%

Great job!

-------------------------------------------------------


04/12/16 14:07 

Problem 1 (20 points):
Score += 20

Problem 2 (15 points):
Score += 15

Problem 3 (10 points):
Score += 10

Problem 4 (5 points):
NameError: global name 'plt' is not defined

Total score: 45/50 = 90.0%

Great job!


Comments:
	need to import plt!

-------------------------------------------------------


