import numpy as np
from scipy.fftpack import fft
from matplotlib import pyplot as plt

def arnoldi(b, Amul, k, tol=1E-8):
    '''Perform `k' steps of the Arnoldi iteration on the linear 
        operator defined by `Amul', starting with the vector 'b'.
    '''
    Q = np.empty((len(b), k+1), dtype=np.complex128)
    H = np.zeros((k+1, k), dtype=np.complex128)
    Q[:, 0] = b / np.linalg.norm(b)
    for j in xrange(k):
        Q[:,j+1] = Amul(Q[:,j])
        for i in xrange(j+1):
            H[i,j] = np.dot(Q[:,i].conjugate().T, Q[:,j+1])
            Q[:,j+1] = Q[:,j+1] - np.dot(H[i,j], Q[:,i])
        H[j+1,j] = np.linalg.norm(Q[:,j+1])
        if np.abs(H[j+1, j]) < tol:
            return H[:j+1, :j+1], Q[:, :j+1]
        Q[:,j+1] = Q[:,j+1]/H[j+1,j]
    return H[:-1, :], Q

def test1():
    A = np.array([[1,0,0],[0,2,0],[0,0,3]])
    Amul = lambda x: A.dot(x)
    H, Q = arnoldi(np.array([1,1,1]), Amul, 3)
    print np.allclose(H, np.conjugate(Q.T).dot(A).dot(Q))
    H, Q = arnoldi(np.array([1,0,0]), Amul, 3)
    print H
    print np.conjugate(Q.T).dot(A).dot(Q)

def ritz(Amul, dim, k, iters):
    ''' Find `k' Ritz values of the linear operator defined by `Amul'.
    '''
    b = np.random.random(dim)
    H, Q = arnoldi(b, Amul, iters)
    return np.linalg.eig(H[:k,:k])[0]

def fft_eigs(dim=2**20,k=4):
    '''Return the largest k Ritz values of the Fast Fourier transform
        operating on a space of dimension dim.
    '''
    eigs = ritz(fft, dim, k, 15)
    return eigs

def plot_ritz(A, n, iters):
    ''' Plot the relative error of the Ritz values of `A'.
    '''
    Amul = lambda x: A.dot(x)
    evals = np.sort(np.linalg.eig(A)[0])[:n]
    a_err = np.zeros((n, iters))
    r_err = np.zeros((n, iters))
    
    for k in xrange(1,iters):
        eigs = np.sort(ritz(Amul, A.shape[0], n, k))
        a_err[:k, k-1] = np.abs(evals[:k]- eigs[:k])
        r_err[:k, k-1] = (np.abs(a_err[:k,k-1])/(np.abs(evals[:k])))
    
    for i in xrange(n):
        plt.semilogy(np.arange(1,iters + 1), abserr[i,:])
    
    plt.show()


