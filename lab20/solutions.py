import numpy as np
from sympy import mpmath as mp
from matplotlib import pyplot as plt
from scipy.integrate import quad
from mpl_toolkits.mplot3d import Axes3D #needed for 3d plotting

def singular_surface_plot(f, x_bounds=(-1.,1), y_bounds=(-1.,1.), res=500, threshold=2., lip=.1):
    """ Plots the absolute value of a function as a surface plot """
    
    # space in 'z' values to use to create a lip on the plot
    x = np.linspace(x_bounds[0], x_bounds[1], res)
    y = np.linspace(y_bounds[0], y_bounds[1], res)
    X, Y = np.meshgrid(x, y, copy=False)
    Z = X + 1j*Y
    Z = f(Z)
    Z = np.abs(Z)
    Z[(threshold+lip>Z)&(Z>threshold)] = threshold
    # Do the same thing for the negative restriction on 'z'. 
    Z[(-threshold-lip<Z)&(Z<-threshold)] = -threshold
    # Set anything that is larger to np.nan so it doesn't get plotted. 
    Z[np.absolute(Z) >= threshold + lip] = np.nan
    # Now actually plot the data.
    fig = plt.figure()
    ax = fig.gca(projection='3d')
    surf = ax.plot_surface(X, Y, Z, cmap="coolwarm")
    plt.show()

def test1():
    f = lambda x : 1/ x
    singular_surface_plot(f)

def partial_fractions(p, q):
    """ Finds the partial fraction representation of the rational
        function 'p' / 'q' where 'q' is assumed to not have any repeated
        roots. 'p' and 'q' are both assumed to be numpy poly1d objects.
        Returns two arrays. One containing the coefficients for
        each term in the partial fraction expansion, and another containing
        the corresponding roots of the denominators of each term. """
    c = np.array([p(root)/q.deriv()(root) for root in q.roots])
    return c, q.roots

def test2():
    p = np.poly1d([1., 0., -1])
    q = np.poly1d([1., -2., -29., -42.]) 
    print partial_fractions(p, q)

def cpv(p, q, tol = 1E-8):
    """ Evaluates the cauchy principal value of the integral over the
        real numbers of 'p' / 'q'. 'p' and 'q' are both assumed to be numpy
        poly1d objects. 'q' is expected to have a degree that is
        at least two higher than the degree of 'p'. Roots of 'q' with
        imaginary part of magnitude less than 'tol' are treated as if they
        had an imaginary part of 0. """
    iroots = q.roots[q.roots.imag>tol]
    return np.real(2. * np.pi * 1j * (np.sum(p(iroots)/q.deriv()(iroots))))
def test3():
    p = np.poly1d([1., 0., 0.])
    q = np.poly1d([1., 0., 0., 0., 1.])
    print cpv(p, q)

def count_roots(p):
    """ Counts the number of roots of the polynomial object 'p' on the
        interior of the unit ball using an integral. """
    raise NotImplementedError()
