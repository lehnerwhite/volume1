import numpy as np
from matplotlib import pyplot as plt

def plot_complex(f, xbounds, ybounds, res=401):

    '''Plot the complex function f.
    INPUTS:
    f - A function handle. Should represent a function from C to C.
    xbounds - A tuple (xmin, xmax) describing the bounds on the real part of the domain.
    ybounds - A tuple (ymin, ymax) describing the bounds on the imaginary part of the domain.
    res - A scalar that determines the resolution of the plot (number of points
        per side). Defaults to 401.
    
    OUTPUTS:
    graph of the function f(z)
    '''
    x = np.linspace(xbounds[0], xbounds[1], res)
    y = np.linspace(ybounds[0], ybounds[1], res)
    xmin = np.min((xbounds[0],ybounds[0]))
    xmax = np.min((xbounds[1],ybounds[1]))
    X,Y = np.meshgrid(x,y)
    Z = f(X,Y)
    plt.pcolormesh(X,Y, np.angle(Z), cmap ='hsv', vmin=-np.pi, vmax=np.pi)    
    plt.show()

def test1():
    f = lambda x, y : x + 1j*y
    plot_complex(f, [-1,1], [-1,1])
    f2 = lambda x, y : np.sqrt((x + 1j*y)**2+1)
    plot_complex(f2, (-3,3), (-3,3))

def problem2():
    '''Create the plots specified in the problem statement.
    >>>>>>>Please title each of your plots!!!!<<<<<<<
    Print out the answers to the questions.
    '''
    #part 1
    f = lambda x, y : (x + 1j*y)**2
    plt.title('z^2')
    plot_complex(f, [-1,1],[-1,1])

    f2 = lambda x, y : (x + 1j*y)**3
    plt.title('z^3')
    plot_complex(f2, [-1,1],[-1,1])
    
    f3 = lambda x, y : (x + 1j*y)**4
    plt.title('z^4')
    plot_complex(f3, [-1,1],[-1,1])
    
    print('The degree of the polynomial function is the number '+
            'of times that the colors repeat')
    
    #part 2
    f4 = lambda x, y: (x + 1j*y)**3 - 1j*(x + 1j*y)**4 - 3*(x + 1j*y)**6
    plt.title('z^3 - iz^4 - 3z^6')
    plot_complex(f4, [-1,1],[-1,1])
     
def problem3():
    '''Create the plots specified in the problem statement.
    Print out the answers to the questions.
    '''
    #part 1
    f = lambda x, y : (x + 1j*y)
    plt.title('z')
    plot_complex(f, [-1,1],[-1,1])

    f2 = lambda x, y : 1/(x + 1j*y)
    plt.title('1/z')
    plot_complex(f2, [-1,1],[-1,1])

    print('The second plot is the original plot but mirrored across the horizontal axis')

    #part 2
    f3 = lambda x, y : (x + 1j*y)**-2
    plt.title('z^-2')
    plot_complex(f3, [-1,1],[-1,1])

    f4 = lambda x, y : (x + 1j*y)**-3
    plt.title('z^-3')
    plot_complex(f4, [-1,1],[-1,1])
    
    f5 = lambda x, y : (x + 1j*y)**2 + 1j*(x + 1j*y)**-1 + (x + 1j*y)**-3
    plt.title('z^2 + iz^-1 + x^-3')
    plot_complex(f5, [-1,1],[-1,1])
    
    print('Near the prigin the last two plots are nearly identical. Using the order ' +  
            'of the colors we can try to identify if it is a pole or not.')

def problem4():
    '''For each plot, create the graph using plot_complex and print out the
    number and order of poles and zeros below it.'''
    f1 = lambda x, y: np.e**(x+1j*y)
    plt.title('e^z')    
    plot_complex(f1,[-8,8],[-8,8])
    print('It doesnt look like there are any roots here')

    f2 = lambda x, y: np.tan((x+1j*y))
    plt.title('tan(z)')    
    plot_complex(f2,[-8,8],[-8,8])
    print('It looks like there are infinite roots every cycle at pi')
    
    f3 = lambda x, y: (16*(x+1j*y)**4 + 32*(x+1j*y)**3 + 32*(x+1j*y)**2 + 16*(x+1j*y) + 4) / (16*(x+1j*y)**4 - 16*(x+1j*y)**3 + 5*(x+1j*y)**2)
    plt.title('third function')
    plot_complex(f3,[-1,1],[-1,1])  
    print('It looks like there is a pole of degree 2 at 0, and it looks like there are poles and two zeros at the points 1/2 - i/2 and 1/2 + i/2')

def problem5():
    '''
    For each polynomial, print out each zero and its multiplicity.
    Organize this so the output makes sense.
    '''
    f1 = lambda x, y : -2*(x+1j*y)**7 + 2*(x+1j*y)**6 - 4*(x+1j*y)**5 + 2*(x+1j*y)**4 - 2*(x+1j*y)**3 - 4*(x+1j*y)**2 + 4*(x+1j*y) -4
    plt.title('function 1')
    plot_complex(f1,[-2,2],[-2,2])
    print('Roots: -1, 1/2 + 9i/10, 1/2 - 9i/10, -3/10 + 13i/10, -3/10 - 13i/10, 8/10 + 8i/10, 8/10 - 8i/10. All with multiplicity of one.')
    
    f2 = lambda x, y : (x+1j*y)**6 + 6*(x+1j*y)**6 - 131*(x+1j*y)**5 - 419*(x+1j*y)**4 + 4906*(x+1j*y)**3 - 131*(x+1j*y)**2 - 420*(x+1j*y) + 4900
    plt.title('function 2')
    plot_complex(f2,[-10,10],[-10,10])
    print('Roots: -10, -1, 7, 1/2 + 9i/10, 1/2 - 9i/10. All have multiplicity of one except for 7 and -10 which have multiplicity of two.')

def problem6():
    '''Create the plots specified in the problem statement.
    Print out the answers to the questions.
    '''
    f1 = lambda x, y : np.sin(1/(100*(x+1j*y)))
    plt.title('-1 to 1')
    plot_complex(f1, [-1,1], [-1,1])

    plt.title('-.01 to .01')
    plot_complex(f1, [-.01,.01], [-.01,.01])
    
    print('There is a strange pole at 0')    

    f2 = lambda x, y : (x+y*1j) + 1000*(x+y*1j)**2
    plt.title('-1 to 1')
    plot_complex(f2, [-1,1], [-1,1])
    
    plt.title('-.0022 to .0022')
    plot_complex(f2, [-.0022,.0022], [-.0022,.0022])

    print('There are really two roots here, one at 0 and the other at -1/1000')

def problem7():
    '''Create the plots specified in the problem statement.
    Print out the answers to the questions.
    '''
    f1 = lambda x,y: np.sqrt((x+1j*y))
    plt.title('sqrt(z)')
    plot_complex(f1, [-2,2], [-2,2])
    print('This plots around the real line and the other parts are just transitioning color.')
    
    f2 = lambda x,y: -np.sqrt((x+1j*y))
    plt.title('-sqrt(z)')
    plot_complex(f2, [-2,2], [-2,2])
    print('Similar to the first plot but different colors and this different values.')

def extraCredit():
    '''
    Create a really awesome complex plot. You can do whatever you want, as long as 
    it's cool and you came up with it on your own.
    You can also animate one of the plots in the lab (look up matplotlib animation)
    Title your plot or print out an explanation of what it is.
    '''
    raise NotImplementedError()
