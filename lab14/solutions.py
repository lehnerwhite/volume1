'''
Lab 14 - Newton's Method.
'''

import numpy as np
from matplotlib import pyplot as plt

def Newtons_method(f, x0, Df, iters=15, tol=.002):
    '''Use Newton's method to approximate a zero of a function.
    
    INPUTS:
    f     - A function handle. Should represent a function from 
            R to R.
    x0    - Initial guess. Should be a float.
    Df    - A function handle. Should represent the derivative 
            of `f`.
    iters - Maximum number of iterations before the function 
            returns. Defaults to 15.
    tol   - The function returns when the difference between 
            successive approximations is less than `tol`.
    
    RETURN:
    A tuple (x, converged, numiters) with
    x           - the approximation to a zero of `f`
    converged   - a Boolean telling whether Newton's method 
                converged
    numiters    - the number of iterations the method computed
    '''
    xn = x0
    diff = np.inf
    iterations = 0
    converged = True
    while diff > tol and iterations <= iters:
        xn1 = xn - (f(xn)/Df(xn))
        diff = np.abs(xn1 - xn)
        iterations += 1
        xn = xn1
    if iterations > iters:
        converged = False
    return (xn, converged, iterations)

    
def prob2():
    '''
    Print the answers to the questions in problem 2.
    '''
    print '1.'
    f = lambda x : np.cos(x)
    Df = lambda x : -1 * np.sin(x)
    print Newtons_method(f, 1, Df, tol=1e-5)
    print Newtons_method(f, 2, Df, tol=1e-5)
    print('See third entry in each of above solutions.')
    print '2.'
    x = np.linspace(-4,4,500)
    f = lambda x : (np.sin(x)/x) - x
    Df = lambda x: (-1.*(x**2 + np.sin(x) - x*np.cos(x)))/(x**2)
    plt.plot(x, f(x))
    plt.show()
    print Newtons_method(f, 1, Df, tol=1e-7)
    print '3.'
    f = lambda x : x**9
    Df = lambda x : 9.*x**8
    print Newtons_method(f, 1, Df, tol=.1)
    print("This method converges slowly because ...")
    print '4.'
    f = lambda x: np.sign(x)*np.power(np.abs(x), 1./3)
    Df = lambda x: 1/3. *  np.sign(x)*np.power(np.abs(x), -2./3)
    print Newtons_method(f, .01, Df)
    print("This method does not converge because it is not continuously differentiable.")
#prob2() 

def Newtons_2(f, x0, iters=15, tol=.002):
    '''
    Optional problem.
    Re-implement Newtons method, but without a derivative.
    Instead, use the centered difference method to estimate the derivative.
    '''
    raise NotImplementedError('Newtons Method 2 not implemented')

def plot_basins(f, Df, roots, xmin, xmax, ymin, ymax, numpoints=100, iters=15, colormap='brg'):
    '''Plot the basins of attraction of f.
    
    INPUTS:
    f       - A function handle. Should represent a function 
            from C to C.
    Df      - A function handle. Should be the derivative of f.
    roots   - An array of the zeros of f.
    xmin, xmax, ymin, ymax - Scalars that define the domain 
            for the plot.
    numpoints - A scalar that determines the resolution of 
            the plot. Defaults to 100.
    iters   - Number of times to iterate Newton's method. 
            Defaults to 15.
    colormap - A colormap to use in the plot. Defaults to 'brg'. 
    
    RETURN:
    Returns nothing, but should display a plot of the basins of attraction.
    '''
    xreal = np.linspace(xmin, xmax, numpoints)
    ximag = np.linspace(ymin, ymax, numpoints)
    Xreal, Ximag = np.meshgrid(xreal, ximag)
    Xold = Xreal+1j*Ximag
    for i in xrange(iters):
        Xnew = Xold - (f(Xold)/Df(Xold))
        Xold = Xnew
    I, J = Xnew.shape
    for i in xrange(I):
        for j in xrange(J):
            l = np.zeros(len(roots))
            for k, root in enumerate(roots):
                l[k] = np.abs(Xnew[i][j] - root)
            Xnew[i][j] = np.argmin(l)
    plt.pcolormesh(Xreal, Ximag, Xnew, cmap=colormap)
    plt.show()

def prob5():
    '''
    Using the function you wrote in the previous problem, plot the basins of
    attraction of the function x^3 - 1 on the interval [-1.5,1.5]X[-1.5,1.5]
    (in the complex plane).
    '''
    f = lambda x:x**3-1
    Df = lambda x:3*x**2
    roots = np.array([1,-1j**(1./3), 1j**(2./3)])
    xmin=-1.5
    xmax=1.5
    ymin=-1.5
    ymax=1.5
    plot_basins(f,Df,roots,xmin,xmax,ymin,ymax,numpoints = 700)

#prob5()
